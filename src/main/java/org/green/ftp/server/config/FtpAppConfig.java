package org.green.ftp.server.config;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.UserFactory;
import org.apache.ftpserver.usermanager.impl.WritePermission;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class FtpAppConfig {

    @Bean
    public FtpServer getServer(@Value("${ftp.file.location}") String location,
                                           @Value("${ftp.username}") String username,
                                           @Value("${ftp.password}") String password,
                                           @Value("${ftp.port}") Integer port) throws FtpException {
        // Adding authority to "write" to ftp directory by remote ftp client
        List<Authority> authorities = new ArrayList<>();
        authorities.add(new WritePermission());

        UserFactory userFactory = new UserFactory();
        userFactory.setHomeDirectory(location);
        userFactory.setName(username);
        userFactory.setPassword(password);
        userFactory.setAuthorities(authorities);
        User user = userFactory.createUser();

        FtpServerFactory serverFactory = new FtpServerFactory();
        serverFactory.getUserManager().save(user);
        ListenerFactory factory = new ListenerFactory();

        // set the port of the listener
        factory.setPort(port);
        // replace the default listener
        serverFactory.addListener("default", factory.createListener());

        return serverFactory.createServer();
    }
}
