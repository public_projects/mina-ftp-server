package org.green.ftp.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinaFtpServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinaFtpServerApplication.class, args);
    }
}
